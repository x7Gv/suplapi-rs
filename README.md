# SuplAPI
[<img alt="gitlab" src="https://img.shields.io/badge/gitlab-x7Gv/suplapi-fca326?style=for-the-badge&labelColor=555555&logo=gitlab" height="20">](https://gitlab.com/x7Gv/suplapi-rs)
[<img alt="crates.io" src="https://img.shields.io/crates/v/suplapi.svg?style=for-the-badge&color=fc8d62&logo=rust" height="20">](https://crates.io/crates/suplapi)
[<img alt="docs.rs" src="https://img.shields.io/badge/docs.rs-suplapi-66c2a5?style=for-the-badge&labelColor=555555&logoColor=white&logo=data:image/svg+xml;base64,PHN2ZyByb2xlPSJpbWciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDUxMiA1MTIiPjxwYXRoIGZpbGw9IiNmNWY1ZjUiIGQ9Ik00ODguNiAyNTAuMkwzOTIgMjE0VjEwNS41YzAtMTUtOS4zLTI4LjQtMjMuNC0zMy43bC0xMDAtMzcuNWMtOC4xLTMuMS0xNy4xLTMuMS0yNS4zIDBsLTEwMCAzNy41Yy0xNC4xIDUuMy0yMy40IDE4LjctMjMuNCAzMy43VjIxNGwtOTYuNiAzNi4yQzkuMyAyNTUuNSAwIDI2OC45IDAgMjgzLjlWMzk0YzAgMTMuNiA3LjcgMjYuMSAxOS45IDMyLjJsMTAwIDUwYzEwLjEgNS4xIDIyLjEgNS4xIDMyLjIgMGwxMDMuOS01MiAxMDMuOSA1MmMxMC4xIDUuMSAyMi4xIDUuMSAzMi4yIDBsMTAwLTUwYzEyLjItNi4xIDE5LjktMTguNiAxOS45LTMyLjJWMjgzLjljMC0xNS05LjMtMjguNC0yMy40LTMzLjd6TTM1OCAyMTQuOGwtODUgMzEuOXYtNjguMmw4NS0zN3Y3My4zek0xNTQgMTA0LjFsMTAyLTM4LjIgMTAyIDM4LjJ2LjZsLTEwMiA0MS40LTEwMi00MS40di0uNnptODQgMjkxLjFsLTg1IDQyLjV2LTc5LjFsODUtMzguOHY3NS40em0wLTExMmwtMTAyIDQxLjQtMTAyLTQxLjR2LS42bDEwMi0zOC4yIDEwMiAzOC4ydi42em0yNDAgMTEybC04NSA0Mi41di03OS4xbDg1LTM4Ljh2NzUuNHptMC0xMTJsLTEwMiA0MS40LTEwMi00MS40di0uNmwxMDItMzguMiAxMDIgMzguMnYuNnoiPjwvcGF0aD48L3N2Zz4K" height="20">](https://docs.rs/suplapi)

Access Nelonen Media Supla API from Rust.

## Currently implemented features
* `supla-playlist.nm-services.nelonenmedia.fi` API
* `prod-component-api.nm-services.nelonenmedia.fi/api/radio-programs` API

## Example(s)

### Query current playlist history
```rs
extern crate suplapi;

let groove_fm = 70;

let supla = suplapi::SuplAPI::<suplapi::http::default::Client>::default();
let playlist = supla.playlist(groove_fm, 20, None).await.unwrap();
assert!(playlist.items.len() == 20);
```

## Credits
The crate has heavily drawn inspiration from the excellent [wikipedia](https://crates.io/crates/wikipedia) crate.
