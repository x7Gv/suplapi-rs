extern crate suplapi;

#[cfg(feature = "http-client")]
mod tests {
    use std::collections::HashMap;

    use suplapi::SuplAPI;
    use suplapi::http;
    use suplapi::data::RadioChannel;


    fn s() -> SuplAPI<http::default::Client> {
        SuplAPI::default()
    }

    #[tokio::test]
    async fn playlist() {
        let suplapi = s();
        let res = suplapi.playlist(RadioChannel::GrooveFM, 20, None).await.unwrap();
        assert!(res.items.len() == 20);
    }

    #[tokio::test]
    async fn program_list() {
        let suplapi = s();
        let res = suplapi.program_list().await.unwrap();

        assert!(!res.get().is_empty());
    }
}
